-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Feb 26, 2024 at 03:12 PM
-- Server version: 8.2.0
-- PHP Version: 8.2.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `2cafe`
--

-- --------------------------------------------------------

--
-- Table structure for table `hotcafeorder`
--

DROP TABLE IF EXISTS `hotcafeorder`;
CREATE TABLE IF NOT EXISTS `hotcafeorder` (
  `id` int NOT NULL AUTO_INCREMENT,
  `hot_cafe_id` int NOT NULL,
  `first_name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `last_name` varchar(25) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `phone_number` varchar(25) NOT NULL,
  `percentage` varchar(25) NOT NULL,
  `payment` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `hotcafeorder`
--

INSERT INTO `hotcafeorder` (`id`, `hot_cafe_id`, `first_name`, `last_name`, `phone_number`, `percentage`, `payment`) VALUES
(1, 3, 'seam', 'somoun', '01234567', '50', '1.23'),
(2, 3, 'seam', 'somoun', '01234567', '50', '1.23'),
(3, 3, 'seam', 'somoun', '01234567', '50', '1.23');

-- --------------------------------------------------------

--
-- Table structure for table `hotcate`
--

DROP TABLE IF EXISTS `hotcate`;
CREATE TABLE IF NOT EXISTS `hotcate` (
  `id` int NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `hotcate`
--

INSERT INTO `hotcate` (`id`, `image`, `name`, `price`) VALUES
(1, 'cafe_image/Oops!.jpg', '', ''),
(2, 'cafe_image/Oops!.jpg', '', ''),
(3, 'cafe_image/Oops!.jpg', 'sdf', '1.23'),
(4, 'cafe_image/Oops!.jpg', 'ASD', '3'),
(5, 'cafe_image/Easy Homemade Caramel Iced Coffee.jpg', 'ASD', '2'),
(6, 'cafe_image/Oops!.jpg', 'ha', '2');

-- --------------------------------------------------------

--
-- Table structure for table `icecafe`
--

DROP TABLE IF EXISTS `icecafe`;
CREATE TABLE IF NOT EXISTS `icecafe` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `price` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `icecafe`
--

INSERT INTO `icecafe` (`id`, `name`, `price`, `image`) VALUES
(1, 'ice', '2', 'cafe_image/Easy Homemade Caramel Iced Coffee.jpg'),
(2, 'ice', '3', 'cafe_image/Easy Homemade Caramel Iced Coffee.jpg'),
(3, 'ice', '2', 'cafe_image/Easy Homemade Caramel Iced Coffee.jpg'),
(4, '', '', 'cafe_image/Easy Homemade Caramel Iced Coffee.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `icecafeorder`
--

DROP TABLE IF EXISTS `icecafeorder`;
CREATE TABLE IF NOT EXISTS `icecafeorder` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ice_cafe_id` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `payment` varchar(25) NOT NULL,
  `percentage` varchar(25) NOT NULL,
  `phone_number` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `icecafeorder`
--

INSERT INTO `icecafeorder` (`id`, `ice_cafe_id`, `first_name`, `last_name`, `payment`, `percentage`, `phone_number`) VALUES
(1, '2', 'sin', 'ko', '3', '65', '0789456');

-- --------------------------------------------------------

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
CREATE TABLE IF NOT EXISTS `room` (
  `room_id` int NOT NULL AUTO_INCREMENT,
  `title1` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `description1` varchar(255) NOT NULL,
  `title2` varchar(255) NOT NULL,
  `description2` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL,
  PRIMARY KEY (`room_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `room`
--

INSERT INTO `room` (`room_id`, `title1`, `description1`, `title2`, `description2`, `image`) VALUES
(1, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas eu tellus id sollicitudin. Suspendisse scelerisque ut ante id hendrerit. Maecenas auctor arcu a velit venenatis, vel auctor lorem tristique.', 'Aenean elit lacus, ullamcorper ut porttitor in', 'Aenean elit lacus, ullamcorper ut porttitor in, pretium ac elit. Mauris sollicitudin dui et ipsum sollicitudin blandit. In hendrerit tellus ante, nec egestas nibh tristique sed.', 'booking/room1.jpg'),
(2, 'Lorem ipsum dolor sit amet', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut egestas eu tellus id sollicitudin. Suspendisse scelerisque ut ante id hendrerit. Maecenas auctor arcu a velit venenatis, vel auctor lorem tristique.', 'Aenean elit lacus, ullamcorper ut porttitor in', 'Aenean elit lacus, ullamcorper ut porttitor in, pretium ac elit. Mauris sollicitudin dui et ipsum sollicitudin blandit. In hendrerit tellus ante, nec egestas nibh tristique sed.', 'booking/room2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `room_booking`
--

DROP TABLE IF EXISTS `room_booking`;
CREATE TABLE IF NOT EXISTS `room_booking` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `room_id` varchar(25) NOT NULL,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `phone_number` varchar(25) NOT NULL,
  `date` varchar(25) NOT NULL,
  `time` varchar(25) NOT NULL,
  `duration` varchar(25) NOT NULL,
  `payment` varchar(25) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `room_booking`
--

INSERT INTO `room_booking` (`user_id`, `room_id`, `first_name`, `last_name`, `phone_number`, `date`, `time`, `duration`, `payment`) VALUES
(1, '2', 'seam', 'somoun', '089383034', '2024-02-26', '02:24', '3', '3'),
(2, '2', 'seam', 'somoun', '089383034', '2024-02-26', '02:24', '3', '3');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
