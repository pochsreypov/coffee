<?php
 include 'menubar.php';
 include 'eventstyle.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

   
    <title>EVENT</title>
</head>
<body>

  <div class="container">
    <div class="allevent">
        <h1> Enjoy Discounts and Special Offers at 2's cafe</h1>
        <div class="newyear">
           <div class="text_new">
              <h1 style="margin-bottom: 20px; margin-top:0px; font-family: 'Roboto', sans-serif;">Happy Khmer New year</h1>
              <p>To celebrate Khmer New Year, we are offering a <strong>15% </strong>discount on membership.</p>
              <strong><p>Register now to become a membership</p></strong>
              <a href="register."><button id="register" class="btn btn-primary">Register</button></a>
            </div>
         <img src="./image/new year.jpg" alt=""> 
        </div>
        <div class="valentine">
        <div class="text_valen" style="  margin: 50px 38px 60px 0px;">
              <h1 style="margin-bottom: 20px; margin-top:0px; font-family: 'Roboto', sans-serif;">Happy valentine's day</h1>
              <p>To celebrate Valentine's day, we are offering a <strong>15% </strong>discount on membership</p>
              <strong><p>Register now to become a membership</p>  <a href="register" ><button id="register" class="btn btn-primary" >Register</button></a></strong>
        </div>
            <img src="./image/valentine.jpg" alt="" >
        </div>
    </div>
  </div>

  <footer id="footer">
  <?php include 'contact.php';?>
</footer>
</body>
</html>
