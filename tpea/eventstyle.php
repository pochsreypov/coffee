<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<body>
  <style>
 .container {
  overflow-x: hidden; /* Add this line to hide horizontal overflow of the container */
}
    .newyear img  {
  display: block;
  margin-left: 10px;
  height: 500px;
  width: 590px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
    }
    .newyear{
      padding: 0px 0px 0px 80px;
      background-color: black;
      width: 80%;
      margin: 0px 300px 0px 160px;
      background-image: url(./image/new_year_back.jpg);
      border-radius: 10px;
      display: flex;
      align-items: center;
      margin-bottom: 50px;
    }
    .text_new ,.text_valen{
      background-color: rgba(255, 255, 255, 0.5);
      padding: 50px 20px 60px 20px;
      padding-right: 10px;
      width: 500px;
      font-size: 25px;
      margin-right: 35px;
      font-family: 'Roboto', sans-serif;
     
    
    }
    .newyear:hover{
      box-shadow: 0px 22px 50px 4px #a6a6a6;
      transform: scale(1.02);
    }
    #register {
    background-color: #4CAF50; /* Set the background color */
    color: white; /* Set the text color */
    padding: 10px 20px; /* Set padding around the button */
    border: none; /* Remove the default button border */
    border-radius: 4px; /* Add rounded corners */
    cursor: pointer; /* Add a cursor pointer on hover */
    margin-top: 10px;
  }
  

   .valentine img{
  display: block;
  margin-left: 0px;
  height: 500px;
  width: 600px;
  border-top-right-radius: 10px;
  border-bottom-right-radius: 10px;
   }
   .valentine{

     padding: 0px 0px 0px 80px;
      background-color: black;
      width: 80%;
      margin: 0px 300px 0px 160px;
      border-radius: 10px;
      display: flex;
      align-items: center;
    background-image: url(./image/background_valentine.jpg);
   
   }
   .valentine:hover{
    box-shadow: 0px 22px 50px 4px#ff000066;
    transform: scale(1.02);
   }
   h1{
    text-align: center;
    margin-bottom: 50px;
    margin-top: 50px;
    font-family: 'Roboto', sans-serif;

   }

  </style>
</body>
</html>