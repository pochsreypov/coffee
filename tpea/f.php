<?php
session_start();
include('connection.php');
include 'bookingroomstyle.php';
include 'menubar.php';
$sql = "SELECT * FROM room";
$data = $con->query($sql);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Booking room</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css"
     integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/1a7779252c.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js"></script>
    
</head>
<body>

<p></p>
<strong><h1 style="font-size: 40px; font-family: 'Open Sans SemiBold', 'Arial Black', sans-serif;
      margin: 50px 0px 30px 0px ;
      font-weight: bold;"> ROOM FOR MEETING</h1></strong>

<?php
if ($data->num_rows > 0) {
    while ($row = $data->fetch_assoc()) {
        echo '<div class="table_container">';
        echo '  <div class="all_room" style="background-color: grey;" >';
        echo '    <div class="text_room">';
        echo '       <h1 style="text-align: center; font-size: 40px; font-family: \'Open Sans SemiBold\', \'Arial Black\', sans-serif;"><strong>' . $row['title1'] . '</strong></h1>';
        echo '      <p>' . $row['description1'] . '</p>';
        echo '      <button type="button" class="btn bookingBtn" name="booki" data-toggle="modal" data-target="#roomModal" data-room-id="' . $row['room_id'] . '" style="background-color: green; color: white; border: none; padding: 10px 20px; border-radius: 4px; cursor: pointer;">Booking</button>';
        echo '    </div>';
        echo '  <img src="' . $row['image'] . '" style="display: block; margin-left: 60px;height: 500px;width: 580px;border-top-right-radius: 10px;border-bottom-right-radius: 10px;">';
        echo '</div>';
        echo '</div>';
    }
}
?>

<!-- Modal -->
<div class="modal fade" id="roomModal" tabindex="-1" role="dialog" aria-labelledby="roomModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="roomModalLabel">Booking Form</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="bookingForm" action="booking_form.php" method="post">
          <input type="hidden" id="room_id" name="room_id">
          <div class="form-group">
              <label>Last Name</label>
              <input type="text" class="form-control" placeholder="lastname" name="last_name">
            </div>
            <div class="form-group">
              <label>Phone Number</label>
              <input type="phone" class="form-control" placeholder="Phone number" name="phone_number">
            </div>
             <div class="form-group">
              <label for="text">Choose Time</label>
              <br>
              <div class="form-group">
                      DATE: <input type="date" name="date" class="form-control"><br>
                      TIME:<input type="time" name="time" class="form-control">
             </div>
             </div>
              <div class="form-group">
                  <label for="text">duration</label><br>  
                  1hour pay 2$<input type="checkbox" name="lenght" value="3" class="form-control" style="height: 20px; width:20px;"> 
                  2 hour pay 4$<input type="checkbox" name="lenght" value="6"class="form-control" style="height: 20px; width:20px;"> 
                  3 hour pay 6$<input type="checkbox" name="lenght" value="9"class="form-control" style="height: 20px; width:20px;"> 
              </div>
              <div class="form-group">
                  <label for="text">payment:</label>
                  <input type="text" name="payment" placeholder="0.0$" class="form-control">
              </div>

          <!-- Add more input fields here -->
          <button type="submit" class="btn btn-primary" >Submit</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script>
$(document).ready(function(){
    $('.bookingBtn').click(function(){
        var roomId = $(this).data('room-id');
        $('#room_id').val(roomId);
    });
});
</script>

</body>
</html>
<?php
  $room_id = $_SESSION['booking'];
 if(isset($_POST['submit'])){
  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $phone_number = $_POST['phone_number'];
  $date = $_POST['date'];
  $time = $_POST['time'];
  $duration = $_POST['lenght'];
  $payment = $_POST['payment'];
  // if(isset($_POST['lenght'])){
  //   echo $_POST['lenght'];
  // }
if ($payment == $duration){
  $insert = $con->prepare("INSERT INTO room_booking (room_id,first_name,last_name,phone_number,date,time,duration,payment) VALUE (?,?,?,?,?,?,?,?)");
  $insert->bind_param("ssssssss",$room_id,$first_name,$last_name,$phone_number,$date,$time,$duration,$payment);
  $insert->execute();
  echo "success";
 }else{
  echo "you payment is not accept";
 }
}
