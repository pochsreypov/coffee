<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>food style</title>
</head>
<body>
    <style>
    
        .food_table{
            width: 50%;
            left: 60%;
            min-height:2000px;
            height: auto;
            width: 90%;
            padding:30px 0px 0px 60px;
            margin-left: 60px;
            background-color: #f1f1f1;
            border-radius: 10px;

        }
        .title{
            text-align: center;
            font-size: 40px;
          font-family: "Open Sans SemiBold", "Arial Black", sans-serif;
          font-weight: bold;
          color: #333;
          margin: 20px;
        }
.g{
    margin-left: 100px;
}
        .tuna img{
          height: 300px;
          width: 300px;
          border-radius: 20px;
        }
        .tuna{
            display: inline;
            float: left;
            margin-right: 20px;
            border-radius: 20px;
            background-color: white;
            margin-bottom: 10px;
            padding: 20px 20px 20px 20px ;
            margin-top: 20px;

            
        }
        .tuna p{
        font-size: 20px;
        }
        .tuna button{
            background-color: green;
            color: white;
           
        }
       
        .tuna:hover{
            box-shadow: rgba(50, 50, 93, 0.25) 0px 50px 100px -20px, rgba(0, 0, 0, 0.3) 0px 30px 60px -30px, rgba(10, 37, 64, 0.35) 0px -2px 6px 0px inset;
        }
    </style>
</body>
</html>