
<?php
include 'style.php';
include 'menubar.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="style.php">
     <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
    <title>2's cafe</title>
</head>
<body>
  <div>
  <div class="banner-card" >
    <img src="./cafeshop.jpg" alt="" height="600px" width="1080px" style="margin-top: 20px;">
     <div class="banner-text">
        <h1>Wellcome to cafe 2's </h1>
        <p>Come and join us at our cafe, where every cup tells a story, and every visit is an opportunity to create cherished memories. We look forward to serving you and providing an unforgettable experience that keeps you coming back for more.</p>
     </div>
     </div>

   <div class="cofee">
    <img src="./coffee.jpg" alt="">
     <div class="cofe_text">
     <h1 style="font-size: 50px;">Dalgona coffee </h1>
      <p>Our coffee is a rich and aromatic blend crafted from the finest beans sourced from around the world.</p>
     </div>
   </div>
   <div class="food">
    <img src="./image/spygetti.jpg" alt="">
     <div class="text_spy">
     <h1 style="font-size: 50px;">Spaghetti bolognese </h1>
      <p>Spaghetti Bolognese is a hearty and satisfying dish that has gained popularity worldwide. It offers a combination of carbohydrates from the pasta and protein from the meat, making it a well-rounded meal.</p>
     </div>
   </div>
  </div>
  </div>
<footer id="footer">
  <?php include 'contact.php';?>
</footer>
</body>
</html>