<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
<div class="modal fade" id="ice">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Modal Title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form class="s-form" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" role="form" enctype="multipart/form-data">
            <center><h3>ice cafee</h3></center>
            <h3>Order Coffe</h3>
            <div class="form-group">           
                <label>First Name</label>
                <input type="text" class="form-control" placeholder="firstname" name="first_name">
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control" placeholder="lastname" name="last_name">
            </div>
            <div class="form-group">
                <label>Phone Number</label>
                <input type="phone" class="form-control" placeholder="Phone number" name="phone_number">
            </div>
            <div class="form-group">
                <label for="text">how many percentage of sugar do u like to order (00% to 100%)</label>
                <input type="text" name="percentage">
            </div>
            <div class="form-group">
                <label for="text">product name : <?php echo $ice_cafe_name; ?></label>
            </div>
            <div class="form-group">
                <label for="text">price :<?php echo $ice_cafe_price; ?> </label>
            </div>
            <div class="form-group">
                <label for="text">payment:</label>
                <input type="text" name="payment" placeholder="0.0$">
            </div>
            <button type="submit" class="btn btn-success s-button" name="submit">Order</button>
            </form> 
        </div>
    </div>
</div>
</body>
</html>