
<?php 
session_start();
include 'menubar.php';
include('connection.php');
// echo $_SESSION['get_cafe_id'];
if (isset($_SESSION['get_cafe_id'])) {
    $get_cafe_id = $_SESSION['get_cafe_id'];

    $sql = "SELECT * FROM icecafe WHERE id = $get_cafe_id";
    $data = $con->query($sql);

    if ($data->num_rows > 0) {
        while ($row = $data->fetch_assoc()) {
            $ice_cafe_name = $row['name'];
            $ice_cafe_price = $row['price'];
            $image = $row['image'];
        }
    }
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css"
     integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
     <style>
.container{
  margin-top:80px ;width: 500px; border-radius: 6px; position: absolute;  transform: translate(-50%,-50%);  
  background-color: white;
  left: 60%;
  top: 50%;
  padding-top: 20px;
  margin-bottom: 100px;
 /* bottom: 10%; */
}
.form-group{
  width: 600px;
}
   </style>
</head>
<body>
    <div>
    <h1 style="text-align: center;margin-bottom:20px;">COFFEE ORDER</h1>
    <img src="<?php echo $image; ?>" alt="" width="600px" height="650px" style="border-radius: 30px;margin-left:45px; margin-top:20px;margin-bottom:80px;">
            <div class="container">
              <form class="container" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" role="form" enctype="multipart/form-data">
            <div class="form">
            <div class="form-group">           
                <label>First Name</label>
                <input type="text" class="form-control" placeholder="firstname" name="first_name">
            </div>
            <div class="form-group">
                <label>Last Name</label>
                <input type="text" class="form-control" placeholder="lastname" name="last_name">
            </div>
            <div class="form-group">
                <label>Phone Number</label>
                <input type="phone" class="form-control" placeholder="Phone number" name="phone_number">
            </div>
            <div class="form-group">
                <label for="text">how many percentage of sugar do u like to order (00% to 100%)</label>
                <input type="text" name="percentage" placeholder="percentage" class="form-control">
            </div>
            <div class="form-group">
                <label for="text">product name : <?php echo $ice_cafe_name; ?></label>
            </div>
            <div><img class="s_image" src="<?php echo $image; ?>" alt="" width="180px" height="
            180px"></div>
            <div class="form-group">
                <label for="text">price :<?php echo $ice_cafe_price; ?> $</label>
            </div>
            <div class="form-group">
                <label for="text">payment:</label>
                <input type="text" name="payment" placeholder="0.0$" class="form-control">
            </div>
            <button style="margin-bottom: 50px;" type="submit" class="btn btn-success s-button" name="submit" >Order</button>
            </form>
        </div>
</div>
  <footer>
    <?php include 'contact.php';?>
  </footer>
</body>
</html>
<?php
if (isset($_POST['submit'])){
    $get_cafe_id = $_SESSION['get_cafe_id'];
    $first_name=$_POST['first_name'];
    $last_name=$_POST['last_name'];
    $phone_number=$_POST['phone_number'];
    $percentage=$_POST['percentage'];
    $payment=$_POST['payment'];
// echo $percentage;
 if ($payment == $ice_cafe_price){
  $insert = $con->prepare("INSERT INTO icecafeorder (ice_cafe_id,phone_number,first_name,last_name,percentage,payment) VALUE (?,?,?,?,?,?)");
  $insert->bind_param("isssss",$get_cafe_id,$phone_number,$first_name,$last_name,$percentage,$payment);
  $insert->execute();
  echo '<script>alert("Your ordering successful"); window.location.href = "#";</script>';
    }else{
      echo "your payment is wrong !";
    }
}
 ?>


