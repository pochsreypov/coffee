
<?php
include 'style.php';

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link rel="stylesheet" href="style.php">
     <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css">
     <link rel="icon" href="malogo.php">
    <title>2's cafe</title>
</head>
<body>
  <div class="menu_bar">
    <a href="homepage.php" class="logo_hover"><img src="malogo.jpg" alt="" height="90px" width="100px" class="image" style="border-radius: 50px; margin-left: 100px;"></a>
    <ul class="ul">
        <li><a href="homepage.php">HOME</a></li>
        <li><a href="#">MENU <i class="fa-solid fa-caret-down"></i></a>
        <div class="drop">
            <ul class="choice">
                <li><a href="foodmenu.php">Food</a></li>
                <li><a href="ice_cafe.php">Coffee</a></li>
            </ul>
          </div>
      </li>
        <li><a href="bookingroom.php">BOOKING</a></li>
        <li><a href="event.php">EVENT</a></li>
        <li><a href="#footer">ABOUT</a></li>
        <li style="margin-right:0px;"><a href="#footer">CONTACT</a></li>
        <li><a href="" style="margin-right: 70px;"><i class="fa-solid fa-user"></i></a>
        <div class="drop left">
            <ul class="choices">
                <li><a href="login.php">Login</a></li>
                <li><a href="register.php">Register</a></li>
            </ul>
          </div>
        </li>
    </ul>
    </div>
</body>
</html>