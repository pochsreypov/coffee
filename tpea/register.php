<?php
include 'register_style.php';
include 'menubar.php';
include 'connection.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="Exercise1.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css">

</head>
<body >
  <img style="margin: 50px;" src="./image/Group of People Standing Together.jpg" alt="">
  <div class="container1" style="background-color: white;width: 500px; left:70%;margin-top:50px;">
  <h3 style="text-align: center;">REGISTER </h3>
  <form  action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" role="form" enctype="multipart/form-data">    
        <div class="form-group">
            <label for="username">username</label>
            <input type="text" class="form-control" id="Email" name="username">
          </div>
          <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="Email" name="email">
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input type="password" id="password1" class="form-control" name="password">
          </div>
          <div >
            <button type="submit" class="form-group" name="submit">SIGN UP</button>
          </div>
           <div style="margin-left:110px">
            <h5 class="lines">OR</h5>
          </div>     
          <div class="icon">
           <i class="fa-brands fa-google"></i>
           <i class="fa-brands fa-facebook-f"></i>
           <i class="fa-brands fa-linkedin-in"></i>
          </div>
          <div>
           <h5 class="h5s">Already a user?<a href="login.php" class="login_hover">LOGIN</a></h5>
           
          </div>
      </div>
    </form>
    <footer id="footer">
  <?php include 'contact.php';?>
</footer>
</body>
</html>
<?php
if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {
    // Retrieve the input values from the form
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $sql = "INSERT INTO membership (username, email, password) VALUES (?, ?, ?)";
    $stmt = $con->prepare($sql);
    $stmt->bind_param("sss", $username, $email, $password);
    if ($stmt->execute()) {
      echo '<script>alert("You have become a member"); window.location.href = "homepage.php";</script>';
    } else {
      echo '<script>alert("NO this user name"); window.location.href = "#";</script>';

    }
    $stmt->close();
}
$con->close();
?>