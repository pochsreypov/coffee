<?php
ob_start(); // Start output buffering

if (isset($_GET['redirectTo'])) {
    $redirectTo = $_GET['redirectTo'];
    ob_end_clean(); // Clear the output buffer
    header("Location: $redirectTo");
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    html{
        scroll-behavior: smooth;
    }
*{
    text-decoration: none;
    padding: 0;
    margin: 0;
    list-style: none;
    box-sizing: border-box;
}
.menu_bar{
    background-color: rgb(5, 116, 5);
    height: 85px;
    padding-top: 0px;
   
}
.menu_bar ul{
    float: right;
   /* margin-right: 200px;*/
   
}
.menu_bar ul li {
    display: inline-block;
    line-height: 80px;
    margin: 0 40px;   
    position: relative;
}
.menu_bar ul li a{
    color: white;
    font-size: 17px;
    padding: 7px 13px ;
    border-radius: 5px;
    text-decoration: none;
}
.drop{
    display: none;
}
.menu_bar ul li:hover .drop{
    transition: 0.5;
    display: block;
    position: absolute;
    left: 0;
    top: 100%;
    background-color: rgb(5, 116, 5);
    z-index: 9999;
    text-decoration: none;
}
 .menu_bar  a.logo_hover:hover{
    background-color: initial ;
 }


.drop ul{
  display: block;
  margin: 5px;
  z-index: 2;
 
}
.drop ul li{
   
    width: 100px;
    padding: 10px;
    right:30px;
    
   
}
.drop a{
    color: white;
    margin-top: 0px;
}
a:hover{
    background-color: black ;
    transition: .5s;
    text-decoration: none;
    width: 100%;

}
.fa-bars{
    font-size: 40px;
    height: 50px;
}

.banner-card img{
    padding: 0px 50px 0px 50px;
    border-radius: 5px;
    width: 100%;
    height:610px ;
    
}
.banner-card{ 
    position:relative;
    padding-bottom: 30px;
}/* donm nang oy dive thom */
.banner-card:hover{
    box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px;
    transform: scale(1.02);
}
.banner-text{
      position: absolute;
      top: 50%;
      left: 50%;
      transform: translate(-50%, -50%);
      color: white;
      font-size: 24px;
      text-align: center;
      background-color: rgba(0, 0, 0, 0.5);
      padding: 10px;
}/*text bos page first ke */
.cofee img{
    width: 40%;
    height: 600px ;
    padding: 0px 50px 0px 0px;
    margin-left: 0px;
    position: relative; 
    z-index: 1;
}/* kleng pic coffee*/
.cofe_text{
      position: absolute;
      top: 50%;
      left: 70%;
      transform: translate(-50%, -50%);
      color: white;
      font-size: 24px;
      text-align: center;
      background-color: green;
      padding: 10px;
      z-index: 2;
}/* text bos coffee*/
.cofee{
    position: relative;
    background-color: green;
    left: 3%;
    right: 30%;
    width: 94% ;
    margin-bottom: 30px;
    

}/* class dive bos coffee*/
.cofee:hover{
    box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px;
    transition: 0.001s;
    transform: scale(1.02);
}
.text_spy{
  
      position: absolute;
      top: 35%;
      left: 10%;  
      right: 50%;
      color: white;
      font-size: 20px;
      text-align: center;
      background-color:  #808080;
      padding: 10px;
      

}/* text bos spygetti*/
.food img{
    width: 40%;
    height: 600px ;
    padding: 0px 0px 0px 30px;
    margin-left: 865px;
}
.food{
    position: relative;
    background-color: #808080;
    left: 0%;
    margin-left: 40px;
    margin-right: 40px;
    margin-bottom: 50px;
}
.food:hover{
    box-shadow: rgba(0, 0, 0, 0.3) 0px 19px 38px, rgba(0, 0, 0, 0.22) 0px 15px 12px;
    transition: 0.001s;
    transform: scale(1.02);
}
/* .menu_bar ul li:hover .drop{
    transition: 0.5;
    display: block;
    position: absolute;
    left: 0;
    top: 100%;
    background-color: rgb(5, 116, 5);
    z-index: 9999;
    text-decoration: none;
}
.drop ul{
  display: block;
  margin: 0px;
  z-index: 2;
 
}
.drop ul li{ 
    width: 100px;
    padding: 10px;
    right:10px;
    
   
}
.drop a{
    color: white;
    margin-top: 0px;
} */
.menu_bar ul li:hover .drop {
    transition: 0.5s;
    display: block;
    position: absolute;
    right: 0; /* Change from left: 0; to right: 0; */
    top: 100%;
    background-color: rgb(5, 116, 5);
    z-index: 9999;
    text-decoration: none;
    padding-left:150px ;
}

.drop ul {
    display: block;
    margin: 0px;
    z-index: 2;
}

.drop ul li {
    width: 100px;
    padding: 0px;
    right: 0;
    left: 10px; /* Change from right: 10px; to right: 0; */
}

.drop a {
    color: white;
    margin-top: 0px;
}
.drop.left {
  left: auto;
  right: 0;
}

.drop ul li {
  text-align: left;
}
@media(max-width: 1535px){
   header{
    padding: 15px 3%;
    transition: .2s;
   }
}
</style>
</body>
</html>